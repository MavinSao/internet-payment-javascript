setInterval(
    function(){
        var d = new Date();
        var t = d.toLocaleTimeString();
        var cr = d.toDateString()
        document.getElementById('Time').innerHTML = t;
        document.getElementById('Date').innerHTML = cr;
    },1000) 
let count = 0
function active(){
    if(count == 0){
        start();
        document.getElementById('btn').innerHTML = '<i class="far fa-stop-circle">&nbsp;Stop</i> ';
        document.getElementById('btn').className = "btn btn-rounded btn-primary btn-danger";
        count=1;
    }else if(count==1){
        stop();
        document.getElementById('btn').innerHTML = '<i class="far fa-trash-alt">&nbsp;Clear</i>';
        document.getElementById('btn').className = "btn btn-rounded btn-danger btn-amber";
        count=2;
    }else if(count==2){
        clear();
        document.getElementById('btn').innerHTML = '<i class="far fa-play-circle">&nbsp;Start</i>';
        document.getElementById('btn').className = "btn btn-rounded btn-info btn-primary";
        count=0;
    }   
}   
var st ;
function start(){
    st_ms =  Date.now();
    st_date = new Date(st_ms); 
    st_time = st_date.toLocaleTimeString().split(":");
    document.getElementById('start').innerHTML = " Start at " + st_time[0] + ":" + st_time[1];
    st =st_ms;
} 

function stop(){
    var et_ms =  Date.now();
    var et_date = new Date(et_ms); 
    var et_time = et_date.toLocaleTimeString().split(":");  
    document.getElementById('stop').innerHTML = " Stop at " + et_time[0] + ":" + et_time[1];
    var cal = totalTime(st,et_ms)
    document.getElementById('totalmoney').innerHTML = "&nbsp;"+ totalMoney(cal) +" Real";
}
function clear(){
    document.getElementById('start').innerHTML = " Start at 00:00";
    document.getElementById('stop').innerHTML = " Stop at 00:00";
    document.getElementById('totaltime').innerHTML ="&nbsp; 0 Minute";
    document.getElementById('totalmoney').innerHTML = "&nbsp; 0 Real";
}

function totalTime(st,et){
    var cal =Math.round(((et-st)/1000)/60);
    var nt; 
    var tt=0;
    
    if(cal<=1){
        tt=1;
    }else tt = cal;

    document.getElementById('totaltime').innerHTML ="&nbsp;" + tt + " Minute";

    return cal;

}
function totalMoney(cal){
    var money = 0;
	    if(cal>=60){
	       return  money = 1500 + totalMoney(cal-60);
	    }
	    else if(cal>=30){
	       return  money = 1500;
	    }
	    else if(cal>=15){
	       return  money = 1000;
	    }
	    else if(cal>=0){
	       return  money = 500;
	    }
	    return  money;
}